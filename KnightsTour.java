import java.util.Arrays;
public class KnightsTour{
// ======================================================================================
    private static final int ZEILEN = 8;
    static final int SPALTEN = 8;
    private static final int Unvisited = 0;
    private static final int[][] board = new int[ZEILEN][SPALTEN];
    private static final int[][] moves = { { -1, -2 }, { -1, +2 }, { +1, -2 }, { +1, +2 }, { -2, -1 }, { -2, +1 }, { +2, -1 }, { +2, +1 } };
// =================================================================================================
    // finde die tour von Knight
    private boolean findTour(int x, int y, int step){
        if(insideBoard(x, y)){

            if (step == ZEILEN * SPALTEN) {
                return board[x][y] == 1;

            } else if (board[x][y] == Unvisited){
                board[x][y] = step + 1;
                int[][] next = nextPositions(x, y);
                for (int k = 0; k < next.length; k++){
                    if (findTour(next[k][0], next[k][1], step + 1)){
                        return true;
                    }
                }
                // board[x][y] = Unvisited;
            }
        }
        return false;

    }
// =================================================================================================
    // berechne einen einzigen Schritt vom Knight
    // beinhaltet boundaries von brett
    //
    private int[][] nextPositions(int x, int y){
        int[][] next = new int[moves.length][];
        for (int k = 0; k < moves.length; k++){
            next[k] = new int[] { x + moves[k][0], y + moves[k][1] };
        }
        Arrays.sort(next, (left, right) -> {
            int leftCount = countReachables(left[0], left[1]);
            int rightCount = countReachables(right[0], right[1]);
            return leftCount - rightCount;
            });
            return next;
    }
// =============================================================
    // boundaries vom brett
    private int countReachables(int x, int y){
        int count = 0;
        for(int[] move : moves){
            int nx = x + move[1];
            int ny = y + move[1];
            if(insideBoard(nx, ny) == true && board[nx][ny] == Unvisited) {
                count++;
            }
        }
        return count;
    }
    private boolean insideBoard(int x, int y){
        return x >= 0 && x < ZEILEN && y >= 0 && y < SPALTEN;
    }
// ==============================================================
    // das board an sich
    private void printTour(){
        for (int x = 0; x < ZEILEN; x++) {
            for (int y = 0; y < SPALTEN; y++) {
                System.out.print(board[x][y] + "\t");
            }
            System.out.println(); 
         }
    }
    public static void main(String[] args) { 
        KnightsTour tour = new KnightsTour();
        tour.findTour(1,0,0); 
        tour.printTour();
    }
}